package("pahil")
	set_homepage("https://gitlab.com/dvdkon/pahil")
    set_description("Cross-platform hooking/patching library")
	add_urls("https://gitlab.com/dvdkon/pahil.git")
    add_versions("2020.05.11", "7990c2fea504a26a69f7ad3dd2ee9224f8073728")
	add_deps("zydis", "asmjit")
    on_install(function(pkg)
        import("package.tools.xmake").install(pkg)
		-- The xmake package helper dumps all header files into one directory,
		-- so we need to copy them separately
		os.rmdir(pkg:installdir("include"))
		os.cp("inc/*", pkg:installdir("include"))
    end)
