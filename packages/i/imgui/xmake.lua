-- Modified version of https://github.com/xmake-io/xmake-repo/blob/master/packages/i/imgui/xmake.lua
package("imgui")
    set_homepage("https://github.com/ocornut/imgui")
    set_description("Bloat-free Immediate Mode Graphical User interface for C++ with minimal dependencies")
    add_urls("https://github.com/ocornut/imgui/archive/$(version).zip",
             "https://github.com/ocornut/imgui.git")
    add_versions("v1.76", "283a41ad534d2d3d7d480a6b6e2f16cc039c26444bfaaab2fea62a0bdeb07bd0")
	add_patches("v1.76", "sdl_include_path.patch", "703b34ea9c25a07dbeb486d7c81871b34f81b988a3a3a09711ab79ec5863be68")
    on_install("linux", "windows", function(package)
        io.writefile("xmake.lua", [[
            target("imgui")
                set_kind("static")
                add_files("*.cpp")
                add_headerfiles("imgui.h", "imconfig.h")
				add_includedirs(".")
				if is_plat("linux") then
					add_packages("sdl2")
					add_files("examples/imgui_impl_opengl3.cpp",
							  "examples/imgui_impl_glfw.cpp",
							  "examples/imgui_impl_sdl.cpp")
					add_headerfiles("examples/imgui_impl_opengl3.h",
									"examples/imgui_impl_glfw.h",
									"examples/imgui_impl_sdl.h")
					add_cxxflags("-fPIC")
				elseif is_plat("windows") then
					add_files("examples/imgui_impl_dx12.cpp",
							  "examples/imgui_impl_dx11.cpp",
							  "examples/imgui_impl_win32.cpp")
					add_headerfiles("examples/imgui_impl_dx12.h",
							  	    "examples/imgui_impl_dx11.h",
									"examples/imgui_impl_win32.h")
				end
        ]])
        import("package.tools.xmake").install(package)
    end)
