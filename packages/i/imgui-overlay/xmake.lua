package("imgui-overlay")
	set_homepage("https://gitlab.com/dvdkon/imgui-overlay")
    set_description("Library for creating ImGui overlays")
	add_urls("https://gitlab.com/dvdkon/imgui-overlay.git")
    add_versions("2020.05.11", "b1ca7637297f8477e1decee266d3382eab2aca11")
	add_deps("imgui")
	if is_plat("linux") then
		add_deps("pahil")
	elseif is_plat("windows") then
		add_deps("minhook")
	end
    on_install(function(pkg)
        import("package.tools.xmake").install(pkg)
    end)
