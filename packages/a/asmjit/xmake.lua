package("asmjit")
	add_urls("https://github.com/asmjit/asmjit.git")
	add_versions("5d40561d14f93dc45613bfa03155d1dfb4f5825a", "5d40561d14f93dc45613bfa03155d1dfb4f5825a")
	on_install("macosx", "linux", "windows", function(pkg)
		-- Workaround to stop IDEs from complaining
		os.cp("test", path.join(pkg:installdir("include"), ".."))
		import("package.tools.cmake").install(pkg, {
			"-DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true",
			"-DASMJIT_STATIC=true",
			"-DASMJIT_NO_JIT=true",
			"-DASMJIT_NO_COMPILER=true",
		})
	end)