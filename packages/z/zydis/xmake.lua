package("zydis")
	add_urls("https://github.com/zyantific/zydis/archive/v$(version).zip")
	add_versions("2.0.3", "b2cfdb433a78ca3fc015156e21852167588e07c2e3e04e521eeaa5dd099b324f")
	on_install("macosx", "linux", "windows", function(pkg)
		import("package.tools.cmake").install(pkg, {
			"-DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true",
		})
	end)